<?php
    // DB Params
    define('DB_HOST', '172.30.200.26');
    define('DB_USER', 'shareposts');
    define('DB_PASS', 'password');
    define('DB_NAME', 'shareposts');
    
    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));
    // URL Root
    define('URLROOT', 'http:// . $_SERVER[\'SERVER_NAME\'] . /shareposts');
    // Site Name
    define('SITENAME', 'SharePosts');
    // App version
    define('VERSION', '1.1.0');
